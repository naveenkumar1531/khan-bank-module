import axios from 'axios'
import { setTableValues, setLoaderStatus, filterTableValues } from '../actions/TableActions'
import { setDialogOpen, setDialogClose } from '../actions/deleteActions'
import { setFormValues } from '../actions/addActions'
export function refreshTableState() {
    return (dispatch, getState) => {
        var state = getState()
        var url = window.hideUnhideTableValuesApiUrl()
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    var tableValues = parseResponseForTableData(result);

                    dispatch(setTableValues(tableValues))
                    dispatch(setLoaderStatus(true))
                },
                (error) => {
                    dispatch(setLoaderStatus(false))
                }
            )
    }
}

export function parseResponseForTableData(jsonData) {
    var data = jsonData;
    var tableData = [];

    Object.keys(data).map((keyName, keyIndex) => {
        tableData[keyIndex] = {};
        tableData[keyIndex].id = data[keyIndex].id;
        tableData[keyIndex].campaignId = data[keyIndex].campaignId;
        tableData[keyIndex].campaignName = data[keyIndex].campaignName;
        tableData[keyIndex].nodeName = data[keyIndex].nodeName;
        tableData[keyIndex].nodeId = data[keyIndex].nodeId;
        tableData[keyIndex].subNodeName = data[keyIndex].subNodeName;
        tableData[keyIndex].subNodeId = data[keyIndex].subNodeId;
        tableData[keyIndex].dateRange = data[keyIndex].startDate + " - " + data[keyIndex].endDate;
        console.log("valueof is enabled at keyIndex " + keyIndex);
        console.log(data[keyIndex].isEnabled);
        tableData[keyIndex].isEnabled = data[keyIndex].isEnabled
    })

    return tableData;
}


export function deleteValues(id) {
    return (dispatch, getState) => {
        var state = getState()
        var url = window.deleteValuesUrl()
        axios.get(url, {
            params: { command: 'deleteFromHideUnhideModuleStateData', Id: id }
        })
            .then((response) => {

            })
            .catch((error) => {
                console.log(error);
            })
            .then(() => {
                dispatch(deleteDialogClose());
            });
    }
}

export function deleteDialogOpen() {
    return (dispatch, getState) => {
        var state = getState()
        dispatch(setDialogOpen());
    }
};

export function deleteDialogClose() {
    return (dispatch, getState) => {
        var state = getState()
        dispatch(setDialogClose());

    }
};

export function searchValues(value) {
    return (dispatch, getState) => {
        var state = getState()
        var tableValues = state.tableReducer !== undefined && state.tableReducer.tableValues !== undefined ? state.tableReducer.tableValues : [];
        if (value !== undefined && value !== "") {
            var filteredValues = filterValues(value, tableValues)
            dispatch(filterTableValues(filteredValues))
        } else {
            dispatch(filterTableValues(tableValues))
        }
    }
}

export function filterValues(value, tableValues) {
    var lowSearch = value.toLowerCase();
    var values = []
    Object.keys(tableValues).forEach(function (key, index) {
        var flag = false;
        if (String(tableValues[key].campaignName).toLowerCase().includes(lowSearch) == true) {
            flag = true;
        }
        if (flag == true) {
            values.push(tableValues[key]);
        }
    });
    return values;
}

export function createFormValues() {
    return (dispatch, getState) => {
        var url = window.createFormValuesUrl()
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    var values = parseResponseForMapData(result);
                    dispatch(setFormValues(values))
                    //this.createCampaignDD();
                    dispatch(setLoaderStatus(true))
                },
                (error) => {
                    dispatch(setLoaderStatus(false))
                }
            )
    }
}

export function parseResponseForMapData(jsonData) {
    var data = jsonData;
    var tableData = [];

    Object.keys(data).map((keyName, keyIndex) => {
        tableData[keyIndex] = {};
        tableData[keyIndex].campaignName = data[keyIndex].campaignName;
        tableData[keyIndex].nodeName = data[keyIndex].nodeName;
        tableData[keyIndex].subNodeName = data[keyIndex].subNodeName;
        tableData[keyIndex].campaignId = data[keyIndex].campaignId;
        tableData[keyIndex].nodeId = data[keyIndex].nodeId;
        tableData[keyIndex].subNodeId = data[keyIndex].subNodeId;
    })

    return tableData;
}

export function addFormValues(values1) {
    return (dispatch, getState) => {
        var fromDate = new Date(values1.fromDate).toISOString().substr(0, 19).replace('T', ' ');
        var toDate = new Date(values1.toDate).toISOString().substr(0, 19).replace('T', ' ');
        var enabled = values1.isEnable == undefined ? false : true
        var url = window.addFormValuesUrl()
        axios.get(url, {
            params: { command: 'addHideUnhideModuleStateData', campaignId: values1.campaign, nodeId: values1.mainMenu, subNodeId: values1.subMenu, startDate: fromDate, endDate: toDate, isEnabled: enabled }
        })
            .then((response) => {
                if(response.status == 200){

                }
            })
            .catch((error) => {
                console.log(error);
            })
            .then(() => {
                //this.handleClose();
            });
    }
}

export function editFormValues (values1,id) {
    return (dispatch, getState) => {
    let editApiParams = makeeditParams(values1,id)
    var url = window.editFormValuesUrl();
    axios.get(url, {
      params: editApiParams
    })
      .then((response) => {
      })
      .catch((error) => {
        console.log(error);
      })
      .then(() => {
        //this.handleClose();
      });
    }
  }

  export function makeeditParams(values1,id){
      let params = {}
      params['command'] = 'updateHideUnhideModuleStateData';
      params['Id'] = id;
      params['startDate'] = values1.fromDate;
      params['endDate'] = values1.toDate;
      params['isEnabled'] = values1.isEnable == undefined ? false : true

      return params
  }