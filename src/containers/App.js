import React from 'react';
// import './App.css';
// import JssProvider from 'react-jss/lib/JssProvider';
// import { create } from 'jss';
// import { createGenerateClassName, jssPreset } from '@material-ui/core/styles';
import NoNodeToHideMessage from "../components/emptyNodeToHide";
import CreateVoiceGuide from "../components/createNodeToHide";
import HideUnhideNodeList from '../components/hideUnhideNodeList'
// const generateClassName = createGenerateClassName();
// const jss = create({
//   ...jssPreset(),
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
//   insertionPoint: document.getElementById('jss-insertion-point'),
// });

function App() {
  return (
    // <JssProvider jss={jss} generateClassName={generateClassName}>
    <div className="App">
      {/* <NoNodeToHideMessage></NoNodeToHideMessage> */}
      <HideUnhideNodeList></HideUnhideNodeList>
    </div>
    // </JssProvider>
  );
}
 
export default App;
