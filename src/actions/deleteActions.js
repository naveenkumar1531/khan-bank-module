export const setDialogOpen = () => {
    return {
        type: 'SET_DIALOG_OPEN',
        value: true

    }
}

export const setDialogClose = () => {
    return {
        type: 'SET_DIALOG_CLOSE',
        value: false

    }
}