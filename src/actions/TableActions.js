export const setTableValues = (tableValues) => {
        return {
                type: 'SET_TABLE_VALUES',
                tableValues

        }
}

export const setLoaderStatus = (value) => {
        return {
                type: 'SET_LOADER_STATUS',
                value

        }
}

export const filterTableValues = (value) => {
        return {
                type: 'SET_FILTER_TABLE_VALUES',
                value

        }
}



