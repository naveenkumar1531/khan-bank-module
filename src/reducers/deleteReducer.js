const deleteReducer=(state={},action) =>{
    switch(action.type){
        case 'SET_DIALOG_OPEN':
        return {
            ...state,
             dialog: action.value
        }

        case 'SET_DIALOG_CLOSE':
        return {
            ...state,
            dialog: action.value
        }
        default:
            return state;
      }
    }
    export default deleteReducer
    