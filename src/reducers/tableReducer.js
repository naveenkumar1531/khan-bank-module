const tableReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_TABLE_VALUES':
            return {
                ...state,
                tableValues: action.tableValues
            }

        case 'SET_LOADER_STATUS':
            return {
                ...state,
                isLoaded: action.value
            }
        case 'SET_FILTER_TABLE_VALUES':
            return {
                ...state,
                filterdTableValues: action.value
            }
        default:
            return state;
    }
}
export default tableReducer
