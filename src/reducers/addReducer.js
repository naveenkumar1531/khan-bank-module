const addReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_FORM_VALUES':
            return {
                ...state,
                formValues: action.value
            }

        default:
            return state;
    }
}
export default addReducer
