import { reducer as formReducer } from 'redux-form'
import {routerReducer } from 'react-router-redux'
import {combineReducers} from 'redux'
import tableReducer from './tableReducer'
import deleteReducer from './deleteReducer'
import addReducer from './addReducer'

export const rootReducer = combineReducers({
 form: formReducer,     
 routing: routerReducer,
 tableReducer,
 deleteReducer,
 addReducer
 });
export default rootReducer