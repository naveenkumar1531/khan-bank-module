
import 'react-app-polyfill/ie9';
import 'babel-polyfill';
import {render} from 'react-dom'
import React from 'react'
import { hashHistory} from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import Root from './containers/Root'
import configureStore from './store/configureStore'

const store = configureStore()
render(
  <Root store={store} />,
  document.getElementById('root')
)

//store.dispatch(getLoggedInUser())
