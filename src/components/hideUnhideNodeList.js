import React, { Component } from 'react';
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import EnhancedTable from './widget/tableWithSort';
import CustomSearch  from './widget/CustomSearch';
import { Grid, Checkbox } from '@material-ui/core';
import CreateNodeToHide  from './createNodeToHide';
import DeleteButton from './widget/deleteButton';
import EditNodeToHide from './editNodeToHide';
import HideCheckbox from './widget/checkBox'
import {refreshTableState}  from '../middlewares/hideUnhideNodesMiddlewares'

class ActionButton extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false
        };
        console.log(this.props.id);
    }

    render() { 
        return ( 
            <React.Fragment>
                <EditNodeToHide id={this.props.id}></EditNodeToHide>
                <DeleteButton id={this.props.id}></DeleteButton>
            </React.Fragment>
        );
    }
}



class HideUnhideNodeList extends Component {
    
    constructor(props) {
        super(props);
        this.state = { 
            fetchedStatedata: [],
         };
      }
    
      componentDidMount() {
        this.props.refreshTableState();
      }
    
      componentDidUpdate() {
        //this.refreshTableState();
      }

    render() {
        const tableHeaders = window.tableHeaders();
        var tableValues = this.props.tableValues;
        Object.keys(tableValues).map((keyName, keyIndex) => {
            tableValues[keyIndex].isEnabled === "t" ? tableValues[keyIndex].isEnabled = <HideCheckbox checked={true} disabled={true} /> : tableValues[keyIndex].isEnabled = <HideCheckbox checked={false} disabled={true} />
            tableValues[keyIndex].actions = <ActionButton id={tableValues[keyIndex].id} />;
        });
        var rows = this.props.filterdTableValues!==undefined? this.props.filterdTableValues : tableValues
        return (
            <React.Fragment>
            <Grid container className="page-layout-spacing mt10 mb10">
                <Grid item sm={2}>
                    <h3 className="left">Hide/Unhide Node ({rows.length})</h3>
                </Grid>
                <Grid item sm={2}>
                    <div className="search-input-field">
                    <CustomSearch></CustomSearch>
                    </div>
                </Grid>
                <Grid item sm={7} className="right-align btn-group">
                <button type="button" className="btn btn-contextual-primary" onClick={this.props.refreshTableState}>Refresh</button>
                    <CreateNodeToHide></CreateNodeToHide>
                </Grid>
            </Grid>
             <EnhancedTable tableData={rows} header={tableHeaders}></EnhancedTable>
             </React.Fragment>
        );
    }
}
const mapStateToProps=(state) => ({
    tableValues : state.tableReducer!== undefined && state.tableReducer.tableValues!== undefined  ? state.tableReducer.tableValues   : [],
    isLoaded:state.tableReducer!== undefined && state.tableReducer.isLoaded!== undefined ? state.tableReducer.isLoaded   : false,
    filterdTableValues : state.tableReducer.filterdTableValues

})

const mapToDispatchProps = (dispatch, ownProps) => ({
	refreshTableState:()=>{
      dispatch(refreshTableState())
    }
})

export default connect(mapStateToProps,mapToDispatchProps)(HideUnhideNodeList)
