import React from 'react'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import FormHelperText from '@material-ui/core/FormHelperText'
import { KeyboardDatePicker } from '@material-ui/pickers';
import Checkbox from '@material-ui/core/Checkbox'


export const renderFromHelper = ({ touched, error }) => {
	if (!(touched && error)) {
		return
	} else {
		return <FormHelperText>{touched && error}</FormHelperText>
	}
}

export const renderTextField = ({
	label,
	input,
	meta: { touched, invalid, error },
	...custom
}) => (
		<TextField
			label={label}
			placeholder={label}
			error={touched && invalid}
			helperText={touched && error}
			{...input}
			{...custom}
		/>
	)

export const renderSelect = ({
	input,
	label,
	meta: { touched, error },
	children,
	...custom
}) => (
		<FormControl error={touched && error}>
			<Select
				native
				{...input}
				{...custom}
				inputProps={{
					name: '',
					id: ''
				}}
			>
				{children}
			</Select>
			{renderFromHelper({ touched, error })}
		</FormControl>
	)

export const DatePicker = ({ input, label, id, aria_label }) => (
	<div>
		<KeyboardDatePicker
			margin="normal"
			id={id}
			label={label}
			{...input}
			onChange={input.onChange}
			KeyboardButtonProps={{
				'aria-label': aria_label,
			}}
		/>
	</div>
)

export const renderCheckbox = ({ input, label }) => (
	<div>
		<FormControlLabel
			control={
				<Checkbox
					checked={input.value ? true : false}
					onChange={input.onChange}
				/>
			}
			label={label}
		/>
	</div>
)
