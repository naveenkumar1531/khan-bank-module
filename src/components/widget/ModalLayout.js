import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
export class ModalLayout extends Component{
  constructor(props){
    super(props);
      this.state = {
        open: false,
        crossIcon:<i class='mdi mdi-close'></i>,
      
  }
}
handleClickOpen = () => {
  this.setState({ open: true });
};

handleClose = () => {
  this.setState({ open: false });
};
  render() {
    const { children,className,modalheading,modalContent,btnPrimary,btnContextualPrimary,btnSecondary,btnContextualSecondary,btnPrimaryContent,btnSecondaryContent,btnPrimaryContextualContent,btnSecondaryContextualContent} = this.props;	
    return (
     <div>
        <Button variant="outlined" className={className} onClick={this.handleClickOpen}>
        {children}
        </Button>
        <Dialog
          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle disableTypography className="modal-header clearfix" id="alert-dialog-title"><h3>{modalheading}</h3>
              <IconButton aria-label="Close" className="closeButton" onClick={this.handleClose}>{this.state.crossIcon}</IconButton>
            </DialogTitle>
          <DialogContent className="modal-content">
            <DialogContentText id="alert-dialog-description">
              <p>{modalContent}
            </p>
            </DialogContentText>
          </DialogContent>
          <DialogActions className="modal-footer clearfix right-align align-right">
            {btnContextualSecondary ?   <Button onClick={this.handleClose} className="btn btn-contextual-secondary">{btnSecondaryContextualContent}</Button> : ''}
            {btnSecondary ?  <Button onClick={this.handleClose} className="btn btn-secondary">{btnSecondaryContent}</Button> : ''}
            {btnContextualPrimary ?  <Button onClick={this.handleClose} className="btn btn-contextual-primary" autoFocus>{btnPrimaryContextualContent} </Button> : ''}
            {btnPrimary ? <Button onClick={this.handleClose} className="btn btn-primary" autoFocus>{btnPrimaryContent} </Button> : ''}
          </DialogActions>
        </Dialog>

    </div>
    );
  }
}


