import React, { Component } from 'react';
import { connect } from 'react-redux'
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import {searchValues} from '../../middlewares/hideUnhideNodesMiddlewares'
class CustomSearch extends Component{
  state = {
    showIcon: false,
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handlefocus = () => {
    this.setState(state => ({ showIcon: !state.showIcon }));
  };

    render() {
              const {id,value,type,className,placeholder} = this.props;
      return (
        <div>
          <TextField 
              id={id}
              value={value}
              onChange={(e) => {this.props.searchValues(e.target.value)}}
              onFocus={this.handlefocus}
              InputProps={{
                disableUnderline:true,
                className:'search-wrapper',
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton className="search-icon">
                      {this.state.showIcon ? <i className="mdi mdi-close"></i> :  <i className="mdi mdi-magnify"></i>}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              

              fullWidth
              type={type}
              
              className={className}
              placeholder={placeholder}
              
            />
          </div>
      );
    }
  }
  const mapStateToProps=(state) => ({
    isLoaded:state.tableReducer!== undefined && state.tableReducer.isLoaded!== undefined ? state.tableReducer.isLoaded   : false
})

const mapToDispatchProps = (dispatch, ownProps) => ({
	searchValues:(value)=>{
    dispatch(searchValues(value))
  }
})

export default connect(mapStateToProps,mapToDispatchProps)(CustomSearch)
