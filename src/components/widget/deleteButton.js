import React, { Component } from 'react';
import { connect } from 'react-redux'
import { CustomIconButton } from './IconButton';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { deleteValues,deleteDialogOpen,deleteDialogClose } from '../../middlewares/hideUnhideNodesMiddlewares'


class DeleteButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            crossIcon: <i className='mdi mdi-close'></i>,
            id: this.props.id
        }
    }
    
    render() {
        return (
            <React.Fragment>
                <CustomIconButton variant="contained" onClick={this.props.deleteDialogOpen}><i className="mdi mdi-delete" /></CustomIconButton>
                <Dialog
                    open={this.props.dialog}
                    onClose={this.props.deleteDialogClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    fullWidth
                >
                    <DialogTitle disableTypography className="modal-header clearfix" id="alert-dialog-title"><h3>Create Voice Guide</h3>
                        <CustomIconButton aria-label="Close" className="closeButton right" onClick={this.props.deleteDialogClose}>{this.state.crossIcon}</CustomIconButton>
                    </DialogTitle>
                    <DialogContent className="modal-content">
                        <p>Are you sure you want to delete this configuration? This cannot be undone.</p>
                    </DialogContent>
                    <DialogActions className="modal-footer clearfix right-align align-right">
                        <Button onClick={(e) => {this.props.deleteDialogClose()}} className="btn btn-secondary">Cancel</Button>
                        <Button onClick={(e) => {this.props.deleteValues(this.props.id)}} className="btn btn-primary" autoFocus>Delete</Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    isLoaded: state.tableReducer !== undefined && state.tableReducer.isLoaded !== undefined ? state.tableReducer.isLoaded : false,
    dialog: state.deleteReducer !== undefined && state.deleteReducer.dialog !== undefined ? state.deleteReducer.dialog : false
})

const mapToDispatchProps = (dispatch, ownProps) => ({
    deleteValues: (id) => {
        dispatch(deleteValues(id))
    },
    deleteDialogOpen: () => {
        dispatch(deleteDialogOpen())
    },
    deleteDialogClose: () => {
        dispatch(deleteDialogClose())
    }
})

export default connect(mapStateToProps, mapToDispatchProps)(DeleteButton)