import React from 'react';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function RadioButton(props) {

  return (
          <FormControlLabel value={props.value} control={<Radio />} label={props.label} />
  );
}