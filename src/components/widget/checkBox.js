import React, { Component } from 'react';
import { Checkbox } from '@material-ui/core';


export class HideCheckbox extends Component {

    constructor(props){
        super(props);
        this.state = { 
            checked: this.props.checked
        };
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }
    
    //handleCheckboxChange = event => this.setState({ checked: event.target.checked })
    handleCheckboxChange() {
        if(this.state.checked === true) {
            this.setState({checked: false}) 
        } else {
            this.setState({checked: true})
        }
    }
    
    render() { 
        return ( 
            <React.Fragment>
                <Checkbox
                style={{padding:'0'}}
                color="primary"
                checked={this.state.checked}
                onChange={this.handleCheckboxChange}
                disabled={this.props.disabled}
                />
            </React.Fragment>
        );
    }
}

export default HideCheckbox