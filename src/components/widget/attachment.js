import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';

class AttachmentWidget extends Component {
    state = {
        file: "",
        fileName: "",
        buttonState: "none"
    }

    setFileData = event => {
        var file = event.target.value;
        this.setState({file: file})
        var revFile = file.split('').reverse().join('');
        var subFileName = revFile.substring(0, revFile.indexOf('\\'));
        var fileName = subFileName.split('').reverse().join('');
        this.setState({fileName: fileName});
        this.setState({buttonState: "inline-block"});
    }
    clearFileUploader = e => {
        this.setState({buttonState: "none"});
        this.setState({file: ""});
        this.setState({fileName: ""});
    }
    render() {
        const {fileName, buttonState} = this.state;
        return (
             <div className="file-upload">
                 <div className="file-uploader">
                     <input type="file" onChangeCapture={this.setFileData}/>
                     <button type="button" className="btn btn-contextual-primary">Upload Customer CSV</button>
                 </div>
                 <div className="uploader-file-name">
                     <span className="uploader-file-name-text">{fileName}</span>
                     <IconButton type="button" className="btn btn-transparent" onClick={this.clearFileUploader} style={{display: buttonState}}>
                         <i className="mdi mdi-close"></i>
                     </IconButton>
                 </div>
                 <div style={{clear: "both"}}></div>
             </div>
        );
    }
}
export default AttachmentWidget;