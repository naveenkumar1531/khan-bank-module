import React, {Component} from "react";
import CreateNodeToHide  from "./createNodeToHide";

class NoNodeToHideMessage extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return(
            <div className="no-data-message">
                <p className="no-data-message__text">It seems you have not added any node to hide from the IVR flow yet!</p>
                <div className="no-data-message__button">
                    <CreateNodeToHide></CreateNodeToHide>
                </div>
            </div>
        );
    }
} 

export default NoNodeToHideMessage;