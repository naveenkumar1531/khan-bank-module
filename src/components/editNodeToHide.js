import 'date-fns';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MenuItem from '@material-ui/core/MenuItem';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import { Grid } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import axios from 'axios';
import { CustomIconButton } from './widget/IconButton';
import HideCheckbox from './widget/checkBox'
import { refreshTableState, editFormValues } from '../middlewares/hideUnhideNodesMiddlewares'
import { renderSelect, DatePicker,renderCheckbox} from './form-elements/FormElements'

const validate = values => {
  const errors = {}
  const requiredFields = [
    'campaign',
    'mainMenu',
    'subMenu',
    'fromDate',
    'toDate'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Please fill out this field'
    }
  })
  return errors
}

class EditNodeToHide extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      crossIcon: <i className='mdi mdi-close'></i>,
      messageTypeValue: "static",
      frequencyValue: "always",
      priorityValue: "1",
      fromDate: "2019-07-18",
      toDate: "2019-07-18",
      isLoaded: false,
      fetchedMapData: [],
      mapData: [],
      id: this.props.id,
      selectedCampaign: '',
      selectedMainMenu: '',
      selectedSubMenu: '',
      initialValues: {},
      DropDownFieldsStyle: { fontSize: '0.8em', marginTop: '16px' }
    }
  }

  handleIsEnabled = event => {
    alert(event);
    this.setState({ isEnabled: event.target.value });
  }


  handleClickOpen = () => {
    var tableValues = this.props.tableValues;
    var initialValues = {}
    Object.keys(tableValues).forEach(function (key, index) {
      if (tableValues[key].id == this.props.id) {
        initialValues['id'] = tableValues[key].id;
        initialValues['campaignId'] = tableValues[key].campaignId;
        initialValues['campaignName'] = tableValues[key].campaignName;
        initialValues['nodeName'] = tableValues[key].nodeName;
        initialValues['nodeId'] = tableValues[key].nodeId;
        initialValues['subNodeName'] = tableValues[key].subNodeName;
        initialValues['subNodeId'] = tableValues[key].subNodeId;
        initialValues['fromDate'] = tableValues[key].startDate;
        initialValues['toDate'] = tableValues[key].endDate;
        initialValues['isEnabled'] = true;
      }
    }.bind(this));
    this.setState({ initialValues: initialValues })
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };


  render() {
    const { classes, values1 } = this.props
    var initialValues = this.state.initialValues
    //alert(JSON.stringify(initialValues))
    var enabled = initialValues.isEnabled;
    let campaigns = [];
    let mainMenus = []
    let subMenus = [];
    campaigns.push(<option value={initialValues.campaignId}>{initialValues.campaignName}</option>)
    mainMenus.push(<option value={initialValues.nodeId}>{initialValues.nodeName}</option>)
    subMenus.push(<option value={initialValues.subNodeId}>{initialValues.subNodeName}</option>)

    return (
      <React.Fragment>
        <CustomIconButton variant="contained" onClick={this.handleClickOpen}><i className="mdi mdi-pencil" /></CustomIconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth
        >
          <DialogTitle disableTypography className="modal-header clearfix" id="alert-dialog-title"><h3>Create Voice Guide</h3>
            <IconButton aria-label="Close" className="closeButton right" onClick={this.handleClose}>{this.state.crossIcon}</IconButton>
          </DialogTitle>
          <DialogContent className="modal-content">
            <Grid container>
              <Grid item sm={12}>
                <InputLabel htmlFor="campaignDDInput" style={this.state.DropDownFieldsStyle}>Campaign *</InputLabel>
                <Field
                  classes={classes}
                  name="campaign"
                  component={renderSelect}
                  label="Select campaign"
                >
                  {campaigns}
                </Field>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={12}>
                <InputLabel htmlFor="mainMenuDDInput" style={this.state.DropDownFieldsStyle}>Node *</InputLabel>
                <Field
                  classes={classes}
                  name="mainMenu"
                  component={renderSelect}
                  label="Select Main Menu"
                >
                  {mainMenus}
                </Field>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={12}>
                <InputLabel htmlFor="subMenuDDInput" style={this.state.DropDownFieldsStyle}>SubNode *</InputLabel>
                <Field
                  classes={classes}
                  name="subMenu"
                  component={renderSelect}
                  label="Select Sub Menu"
                >
                  {subMenus}
                </Field>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={5}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Field
                    name="fromDate"
                    component={DatePicker}
                    label="From"
                    aria_label="'from date'"

                  >
                  </Field>
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={5}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Field
                    name="toDate"
                    component={DatePicker}
                    label="To"
                    aria_label="'to date'"

                  >
                  </Field>
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={2}>
                <table>
                  <tr>
                    <InputLabel style={{ fontSize: '0.8em', marginTop: '16px', float: 'right' }}>Enabled *</InputLabel>
                  </tr>
                  <tr>
                    <HideCheckbox checked={enabled} onChange={this.handleIsEnabled} />
                  </tr>
                </table>

              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions className="modal-footer clearfix right-align align-right">
            <Button onClick={this.handleClose} className="btn btn-secondary">Cancel</Button>
            <Button onClick={(e) => { this.props.editFormValues(this.props.values1,initialValues.id) }} className="btn btn-primary" autoFocus>Apply</Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}
EditNodeToHide = reduxForm({
  form: 'editForm',
  validate,
  destroyOnUnmount: true,
  forceUnregisterOnUnmount: true
})(EditNodeToHide)

EditNodeToHide = connect(
  state => ({
    values1: state.form.editForm !== undefined && (state.form.editForm.values !== undefined ?
      state.form.editForm.values :
      "")
  }))(EditNodeToHide)

const mapStateToProps = (state) => ({
  tableValues: state.tableReducer !== undefined && state.tableReducer.tableValues !== undefined ? state.tableReducer.tableValues : [],
  isLoaded: state.tableReducer !== undefined && state.tableReducer.isLoaded !== undefined ? state.tableReducer.isLoaded : false
})

const mapToDispatchProps = (dispatch, ownProps) => ({
  refreshTableState: () => {
    dispatch(refreshTableState())
  },
  editFormValues: (values1,id) => {
    dispatch(editFormValues(values1,id))
  }
})

export default connect(mapStateToProps, mapToDispatchProps)(EditNodeToHide)


