import 'date-fns';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Grid } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import { createFormValues, addFormValues } from '../middlewares/hideUnhideNodesMiddlewares'
import { renderSelect, DatePicker,renderCheckbox} from './form-elements/FormElements'

const validate = values => {
  const errors = {}
  const requiredFields = [
    'campaign',
    'mainMenu',
    'subMenu',
    'fromDate',
    'toDate'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Please fill out this field'
    }
  })
  return errors
}


class CreateNodeToHide extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      crossIcon: <i className='mdi mdi-close'></i>,
      selectedCampaign: '',
      selectedMainMenu: '',
      selectedSubMenu: '',
      idEnabled: false,
      maineMenus: [],
      subMenus: [],
      DropDownFieldsStyle: { fontSize: '0.8em', marginTop: '16px' }
    }
  }

  componentWillMount() {
    this.props.createFormValues();
  }


  handleCampaignChange = event => {
    this.setState({ selectedCampaign: event.target.value, selectedMainMenu: '', selectedSubMenu: '' });
    var formValues = this.props.formValues;
    var selectedCampaign = event.target.value;
    var mainMenus = []
    mainMenus.push(<option value={''}>Select Option</option>);

    Object.keys(formValues).forEach(function (key, index) {
      if (formValues[key].campaignId == selectedCampaign) {
        mainMenus.push(<option value={formValues[key].nodeId}>{formValues[key].nodeName}</option>)
      }
    })
    this.setState({ mainMenus: mainMenus });
  }



  handleMainMenuChange = event => {
    this.setState({ selectedMainMenu: event.target.value, selectedSubMenu: '' });
    var formValues = this.props.formValues;
    var selectedMainMenu = event.target.value;
    var selectedCampaign = this.state.selectedCampaign;
    var subMenus = []
    subMenus.push(<option value={''}>Select Option</option>);
    Object.keys(formValues).forEach(function (key, index) {
      if (formValues[key].nodeId == selectedMainMenu && formValues[key].campaignId == selectedCampaign) {
        subMenus.push(<option value={formValues[key].subNodeId}>{formValues[key].subNodeName}</option>)
      }
    })

    this.setState({
      subMenus: subMenus
    });
  }

  handleSubMenuChange = event => {
    this.setState({ selectedSubMenu: event.target.value });
  }


  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };



  render() {
    const { classes, values1 } = this.props
    let campaigns = []
    let disabled = true;
    let count = 0;
    if (this.props.formValues !== undefined) {
      Object.keys(values1).forEach(function (key, index) {
        if (values1[key] !== undefined && values1[key] !== "") {
          count++;
        }
        if (count == 5) {
          disabled = false;
        }
      })
      var formValues = this.props.formValues
      campaigns.push(<option value={''}>Select Option</option>);
      Object.keys(formValues).forEach(function (key, index) {
        campaigns.push(<option value={formValues[key].campaignId}>{formValues[key].campaignName}</option>)
      })

    }
    return (
      <React.Fragment>
        <Button variant="outlined" className="btn btn-primary" onClick={this.handleClickOpen}>
          Add Node
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth
        >
          <DialogTitle disableTypography className="modal-header clearfix" id="alert-dialog-title"><h3>Create Voice Guide</h3>
            <IconButton aria-label="Close" className="closeButton right" onClick={this.handleClose}>{this.state.crossIcon}</IconButton>
          </DialogTitle>
          <DialogContent className="modal-content">
            <Grid container>
              <Grid item sm={12}>
                <InputLabel htmlFor="campaignDDInput" style={this.state.DropDownFieldsStyle}>Campaign *</InputLabel>
                <Field
                  classes={classes}
                  name="campaign"
                  component={renderSelect}
                  label="Select campaign"
                  onChange={this.handleCampaignChange}
                >
                  {campaigns}
                </Field>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={12}>
                <InputLabel htmlFor="mainMenuDDInput" style={this.state.DropDownFieldsStyle}>Node *</InputLabel>
                <Field
                  classes={classes}
                  name="mainMenu"
                  component={renderSelect}
                  label="Select Main Menu"
                  onChange={this.handleMainMenuChange}
                >
                  {this.state.mainMenus}
                </Field>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={12}>
                <InputLabel htmlFor="subMenuDDInput" style={this.state.DropDownFieldsStyle}>SubNode *</InputLabel>
                <Field
                  classes={classes}
                  name="subMenu"
                  component={renderSelect}
                  label="Select Sub Menu"
                  onChange={this.handleSubMenuChange}
                >
                  {this.state.subMenus}
                </Field>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={5}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Field
                    name="fromDate"
                    component={DatePicker}
                    label="From"
                    aria_label="'from date'"

                  >
                  </Field>
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={5}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Field
                    name="toDate"
                    component={DatePicker}
                    label="To"
                    aria_label="'to date'"

                  >
                  </Field>
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item sm={2}>
                  <Field
                    name="isEnable"
                    component={renderCheckbox}
                    label="Enabled"
                  >
                  </Field>
              </Grid>
            </Grid>

          </DialogContent>
          <DialogActions className="modal-footer clearfix right-align align-right">
            <Button onClick={this.handleClose} className="btn btn-secondary">Cancel</Button>
            <Button onClick={(e) => { this.props.addFormValues(this.props.values1) }} className="btn btn-primary" disabled={disabled} autoFocus>Apply</Button>
          </DialogActions>
        </Dialog>

      </React.Fragment>
    );
  }
}

CreateNodeToHide = reduxForm({
  form: 'addForm',
  validate,
  destroyOnUnmount: true,
  forceUnregisterOnUnmount: true
})(CreateNodeToHide)

CreateNodeToHide = connect(
  state => ({
    values1: state.form.addForm !== undefined && (state.form.addForm.values !== undefined ?
      state.form.addForm.values :
      "")
  }))(CreateNodeToHide)

const mapStateToProps = (state) => ({
  formValues: state.addReducer !== undefined && state.addReducer.formValues !== undefined ? state.addReducer.formValues : [],
})

const mapToDispatchProps = (dispatch, ownProps) => ({
  createFormValues: () => {
    dispatch(createFormValues())
  },
  addFormValues: (values1) => {
    dispatch(addFormValues(values1))
  }
})

export default connect(mapStateToProps, mapToDispatchProps)(CreateNodeToHide)


