function tableHeaders(){
    var tableHeader = [
        {
            label: "Campaign",
            prop: "campaignName"
        }, 
        {
            label: "IVR Node",
            prop: "nodeName"
        },  
        {
            label: "IVR Subnodes",
            prop: "subNodeName"
        }, 
        {
            label: " Date Range",
            prop: "dateRange"
        }, 
        {
            label: "Hide",
            prop: "isEnabled"
        },
        {
            label: "Actions",
            prop: "actions"
        }
    ];

    return tableHeader;

}


function hideUnhideTableValuesApiUrl(){
    return "http://180.151.75.219:8786/KBHideUnhideModule/v1.php?command=getHideUnhideModuleStateData";
}

function deleteValuesUrl(){
    return 'http://10.10.2.100:8786/KBHideUnhideModule/v1.php';
}
function createFormValuesUrl(){
    return "http://180.151.75.219:8786/KBHideUnhideModule/v1.php?command=getHideUnhideModuleMapData";
}

function addFormValuesUrl(){
    return 'http://10.10.2.100:8786/KBHideUnhideModule/v1.php';
}